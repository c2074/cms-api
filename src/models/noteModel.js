const { Sequelize, DataTypes } = require("sequelize")
/*const sequelize = new Sequelize(
  "postgres://dev:dev123@localhost:5432/clientmanagementsystem"
)*/
const sequelize = new Sequelize("clientmanagementsystem", "dev", "dev123", {
  host: "localhost",
  dialect: "postgres",
  omitNull: true,
  logging: false,
})

const Note = sequelize.define(
  "Note",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    note: {
      type: DataTypes.TEXT,
    },
    client_id: {
      type: DataTypes.INTEGER,
    },
  },
  {
    tableName: "Notes",
  }
)

module.exports = Note
