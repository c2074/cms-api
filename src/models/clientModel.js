const { Sequelize, DataTypes } = require("sequelize")

const sequelize = new Sequelize("clientmanagementsystem", "dev", "dev123", {
  host: "localhost",
  dialect: "postgres",
  omitNull: true,
  logging: false,
})

const Client = sequelize.define(
  "Client",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    first_name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    last_name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    phone_number: {
      type: DataTypes.STRING,
    },
    email_address: {
      type: DataTypes.STRING,
    },
    website: {
      type: DataTypes.STRING,
    },
  },
  {
    tableName: "Clients",
  }
)

module.exports = Client
