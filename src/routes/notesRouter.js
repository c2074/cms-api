const express = require("express")
const router = express.Router()

const controller = require("../controllers/notesController")

router.get("/get", controller.notes_list_get)
router.get("/create", controller.notes_create_get)

module.exports = router
