const clientModel = require("../models/clientModel")

exports.client_list_get = async (req, res) => {
  if (req.query.selectQuery === "all") {
    const users = await clientModel.findAll()
    res.json(users)
  } else if (req.query.selectQuery === "id") {
    const client = await clientModel.findOne({ where: { id: req.query.id } })
    if (client === null) res.send("NOT FOUND")
    else res.json(client)
  }
}

exports.client_create_get = (req, res) => {
  clientModel.sync({ alter: true })
  clientModel.create({
    first_name: req.query.firstName,
    last_name: req.query.lastName,
    phone_number: req.query.phoneNumber,
    email_address: req.query.emailAddress,
    website: req.query.website,
  })

  res.redirect(req.header("Referer"))
}

exports.client_delete_get = (req, res) => {
  clientModel.destroy({
    where: {
      id: req.query.id,
    },
  })
  res.redirect(req.header("Referer"))
}

exports.client_update_get = (req, res) => {
  res.send("NOT IMPLEMENTED: Client update")
}
