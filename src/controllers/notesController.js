const noteModel = require("../models/noteModel")

exports.notes_list_get = async (req, res) => {
  noteModel.sync({ alter: true })
  const notes = await noteModel.findAll({
    attributes: ["note", "id"],
    where: {
      client_id: req.query.client_id,
    },
  })

  res.json(notes)
}

exports.notes_create_get = async (req, res) => {
  noteModel.sync({ alter: true })
  noteModel.create({
    note: req.query.note,
    client_id: req.query.clientId,
  })

  // TODO: Uncomment this line when using with client
  // res.redirect(req.header("Referer"))
  res.redirect(req.header("Referrer"))
}
