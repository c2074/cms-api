# Client Management System API
WARNING! This project is still in development and many features are missing. Contact system administrator to setup this project on a server. Make sure that client is also installed onto same server.

## Upcoming features
For version 1.0 we're aiming to provide system administrators with a `config.json` file that will hold database and PORT information.

## Installation
Just run `npm install && npm start` in directory to install dependencies and get project running. Output in console should be the URL in which the application is listening. Database has to be postgresql. You can edit the credentials in each of the `*Model.js` files until we supply support for a `config.json` file.
